﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="User_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    List User
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <a href="/User/Forms.aspx" class="btn btn-success btn-sm">Add User</a>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Company</th>
                                <th>Position </th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Flag</th>
                                <th>Created By</th>
                                
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="repeaterUser" runat="server" OnItemCommand="repeaterUser_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td class="fitSize"><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("NameCompany") %></td>
                                        <td><%# Eval("NamePosition") %></td>
                                        <td><%# Eval("Name") %></td>
                                        <td><%# Eval("Address ") %></td>
                                        <td><%# Eval("Email") %></td>
                                        <td><%# Eval("Telephone") %></td>
                                        <td><%# Eval("Username") %></td>
                                        <td><%# Eval("Role") %></td>
                                         <td>
                                        <%# Convert.ToInt32(Eval("Flag")) == 1 ? "Aktif" : "Non Aktif" %>
                                    </td>
                                        <td><%# Eval("CreatedBy") %></td>
                                          <td class="text-right fitSize">
                                            <asp:Button ID="Update" CssClass="btn btn-info btn-sm" CommandName="Update" runat="server" Text="Update" CommandArgument='<%# Eval("ID") %>' />
                                            <asp:Button ID="Delete" CssClass="btn btn-danger btn-sm" CommandName="Delete" runat="server" Text="Delete" CommandArgument='<%# Eval("ID") %>' OnClientClick='return confirm("Apakah Anda Yakin Menghapus Data?")' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>
