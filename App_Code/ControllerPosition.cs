﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerPosition : ClassBase
{
    public ControllerPosition(ClassDatabasesDataContext _db) : base(_db)
    {

    }

    //GET DATA
    public Position[] Data()
    {
        return db.Positions.ToArray();
    }

    //Create
    public Position Create(string name,int createdby)
    {
        Position position = new Position
        {
            UID = Guid.NewGuid(),
            Name = name,
            CreatedBy = createdby,
            CreatedAt = DateTime.Now
        };
        db.Positions.InsertOnSubmit(position);
        return position;
    }

    public Position Cari(string UID)
    {
        return db.Positions.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public Position Update(string UID, string name)
    {
        var position = Cari(UID);

        if (position != null)
        {
            position.Name = name;
            return position;
        }
        else
            return null;
    }


    public Position Delete(string uid)
    {
        var position = Cari(uid);
        if (position != null)
        {
            var users = db.Users.Where(x => x.IDPosition == position.ID);
            db.Users.DeleteAllOnSubmit(users);
            db.Positions.DeleteOnSubmit(position);
            db.SubmitChanges();
        }
        return position;
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> position = new List<ListItem>();

        position.Add(new ListItem { Value = "0", Text = "-Pilih-" });
        position.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));
        return position.ToArray();
    }

}