﻿using AjaxControlToolkit.HtmlEditor.ToolbarButtons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.PeerToPeer;
using System.Security.Cryptography;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerUser : ClassBase
{
    public ControllerUser(ClassDatabasesDataContext _db) : base(_db)
    {

    }

    //GET DATA
    public void ViewData()
    {
        var data = db.Users
        .Select(x => new
        {
            x.ID,
            x.IDCompany,
            NameCompany = x.Company.Name,
            x.IDPosition,
            NamePosition = x.Position.Name,
            x.Name,
            x.Address,
            x.Email,
            x.Telephone,
            x.Username,
            x.Password,
            x.Role,
            x.Flag,
            x.CreatedBy,

        }).ToArray();
    }

    //Create
    public User Create(
         int idcompany,
        int idposition,
        string name,
        string address,
        string email,
        string telephone,
        string username,
        string password,
        string role,
        int flag,
        int createdby
        )
    {
        User user = new User
        {
            UID = Guid.NewGuid(),
            IDCompany = idcompany,
            IDPosition = idposition,
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            Username = username,
            Password = password,
            Role = role,
            Flag = flag,
            CreatedBy = createdby,
            CreatedAt = DateTime.Now
        };
        db.Users.InsertOnSubmit(user);
        return user;
    }

    public User Cari(int ID)
    {
        return db.Users.FirstOrDefault(x => x.ID == ID);
    }

    public User Update(int ID, int idcompany, int idposition, string name, string address, string email, string telephone, string username, string password, string role, int flag, int createdby)
    {
        var user = Cari(ID);

        if (user != null)
        {
           user.IDCompany  = idcompany;
           user.IDPosition = idposition;
           user.Name       = name;
           user.Address    = address;
           user.Email      = email;
           user.Telephone  = telephone;
           user.Username   = username;
           user.Password   = password;
           user.Role       = role;
           user.Flag       = flag;
           user.CreatedBy  = createdby;
            return user;
        }
        else
            return null;
    }

    public User Delete(int ID)
    {
        var user = Cari(ID);
        if (user != null)
        {
            db.Users.DeleteOnSubmit(user);
            db.SubmitChanges();
        }
        return user;
    }
}