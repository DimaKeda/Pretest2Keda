﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Company_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="Server">
    List Company
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <a href="Forms.aspx" class="btn btn-success btn-sm">Add New</a>
    <br />
    <br />
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed table-hover table-bordered table-striped">
                    <thead>
                        <tr class="active">
                            `                        
                            <th>No</th>
                            <th>Nama</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Telephone</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="repeaterCompany" runat="server" OnItemCommand="repeaterCompany_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td class="fitSize"><%# Container.ItemIndex +1 %></td>
                                    <td><%# Eval("Name") %></td>
                                    <td><%# Eval("Address") %></td>
                                    <td><%# Eval("Email") %></td>
                                    <td><%# Eval("Telephone") %></td>
                                    <td>
                                        <%# Convert.ToInt32(Eval("Flag")) == 1 ? "Aktif" : "Non Aktif" %>
                                    </td>
                                    <td><%# Eval("CreatedAt") %></td>
                                    
                                    <td class="text-right fitSize">
                                        <asp:Button ID="Update" CssClass="btn btn-info btn-sm" CommandName="Update" runat="server" Text="Update" CommandArgument='<%# Eval("uid") %>' />
                                        <asp:Button ID="Delete" CssClass="btn btn-danger btn-sm" CommandName="Delete" runat="server" Text="Delete" CommandArgument='<%# Eval("uid") %>' OnClientClick='<%# "return confirm(\"Apakah Anda Yakin Menghapus Data? "+ Eval("Name") + "\")" %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" runat="Server">
</asp:Content>

