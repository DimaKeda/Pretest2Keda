﻿using AjaxControlToolkit.HtmlEditor.ToolbarButtons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.PeerToPeer;
using System.Security.Cryptography;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerDocument : ClassBase
{
    public ControllerDocument(ClassDatabasesDataContext _db) : base(_db)
    {

    }

    //GET DATA
    public void ViewData()
    {
        var data = db.Documents
        .Select(x => new
        {
            x.ID,
            x.IDCompany,
            NameCompany = x.Company.Name,
            x.IDCategory,
            NameCategory = x.DocumentCategory.Name,
            x.Name,
            x.Description,
            x.Flag,
            x.CreatedBy
        }).ToArray();
    }

    //Create
    public Document Create(
        int idcompany,
        int idcategory,
        string name,
        string description,
        int flag,
        int createdby
        )
    {
        Document document = new Document
        {
            UID = Guid.NewGuid(),
            IDCompany = idcompany,
            IDCategory = idcategory,
            Name = name,
            Description = description,
            Flag = flag,
            CreatedBy = createdby,
            CreatedAt = DateTime.Now
        };
        db.Documents.InsertOnSubmit(document);
        return document;
    }

    public Document Cari(int ID)
    {
        return db.Documents.FirstOrDefault(x => x.ID == ID);
    }

    public Document Update(int ID, int idcompany, int idcategory, string name, string description, int flag, int createdby)
    {
        var document = Cari(ID);

        if (document != null)
        {
           document.IDCompany  = idcompany;
           document.IDCategory = idcategory;
           document.Name       = name;
           document.Description= description;
           document.Flag       = flag;
           document.CreatedBy  = createdby;
            return document;
        }
        else
            return null;
    }

    public Document Delete(int ID)
    {
        var document = Cari(ID);
        if (document != null)
        {
            db.Documents.DeleteOnSubmit(document);
            db.SubmitChanges();
        }
        return document;
    }
}